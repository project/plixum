<?php

/**
 * @file
 * The Plixum settings for admin.
 */

/**
 * Menu callback for 'admin/config/development/plixum'.
 */
function plixum_admin_settings() {
  $form = array();

  $form['plixum'] = array(
    '#type' => 'fieldset',
    '#title' => t('Plixum settings'),
  );

  $form['plixum']['plixum_count'] = array(
    '#type' => 'select',
    '#title' => t('How many plixum blocks to generate'),
    '#options' => drupal_map_assoc(array(
      1, 3, 5, 7, 9, 10, 15, 20, 25, 30, 40, 50, 60, 70, 80, 90, 100)),
    '#default_value' => plixum_get_settings('plixum_count'),
    '#description' => t('Note: If you go lower than previous amount of blocks, the blocks larger than the amount will be automatically deleted. You can adjust the length of lorem ipsum text per block, and the random images, if provided, via block admin.'),
  );

  $form['plixum']['plixum_image_library'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to image library'),
    '#description' => t('The path to image folder containing images that will be used as option for each Plixum block. If provided, the next generation will include the image it can find alphabetically. You can later adjust individual block to best suit your design.'),
    '#default_value' => plixum_get_settings('plixum_image_library'),
  );

  $form['#submit'][] = 'plixum_admin_settings_form_submit';

  return system_settings_form($form);
}

/**
 * Submit handler for admin_settings_form().
 *
 * Retrieves cache type for each block and saves that instead of `1` for each
 * enabled block.
 */
function plixum_admin_settings_form_submit($form, &$form_state) {
  $data = array();
  $counts = $form_state['values']['plixum_count'];
  $libraries = $form_state['values']['plixum_image_library'];
  $data['plixum_image_library'] = $libraries;
  $data['plixum_count'] = $counts;

  // Merge all settings.
  variable_set('plixum_settings', $data);

  // Copy images into public directory if any.
  if (!empty($libraries)) {
    plixum_copy_images($libraries);
  }

  // Generate blocks if so configured.
  if (!empty($counts)) {

    // Collects blocks larger than current $counts if any.
    $blocks = db_query('SELECT bid FROM {plixum} ORDER BY bid');
    $bids = array();
    foreach ($blocks as $block) {
      $bid = $block->bid;
      if ($bid > $counts) {
        $bids[$bid] = $bid;
      }
    }

    // Delete blocks larger than current $counts.
    if ($bids) {
      db_delete('plixum')
        ->condition('bid', $bids, 'IN')
        ->execute();
      // Leave the rest to _block_rehash().
      drupal_set_message(t('Deleted Plixum blocks: @bids', array('@bids' => implode(", ", $bids))));
    }

    // Build the Plixum blocks.
    $images = plixum_scan_images();
    foreach (range(1, $counts) as $count) {
      // Skip if block already existed.
      if (plixum_block_get_delta($count)) {
        continue;
      }

      // Check images if any to use until overriden.
      $plixum_path     = '';
      $plixum_preset   = '';
      $plixum_position = '';
      if (!empty($images) && !empty($images[$count])) {
        $plixum_path = $images[$count]->uri ? $images[$count]->uri : '';
        $plixum_preset = 'thumbnail';
        $plixum_position = 'left';
      }

      // Store defaults to build basic Plixum blocks.
      db_insert('plixum')
        ->fields(array(
          'bid' => $count,
          'body' => plixum_greeking(40),
          'info' => t('Plixum #@count', array('@count' => $count)),
          'format' => variable_get('filter_fallback_format', ''),
          'options' => serialize(array(
            'plixum_path' => $plixum_path,
            'plixum_preset' => $plixum_preset,
            'plixum_position' => $plixum_position,
            'title' => '',
            'classes' => '',
          )),
        ))
        ->execute();
    }
  }

  // Delete unused variables since we merged them already.
  unset($form_state['values']['plixum_image_library'], $form_state['values']['plixum_count']);
  drupal_set_message(t('Visit <a href="@block_admin">block admin</a> to start working with the Plixum blocks', array('@block_admin' => url('admin/structure/block'))));
}

/**
 * Off-loaded functions.
 */

/**
 * Implements hook_theme().
 */
function _plixum_theme($existing, $type, $theme, $path) {
  $path = drupal_get_path('module', 'plixum');
  $themes = array(
    'plixum' => array(
      'render element' => 'element',
      'template' => 'templates/plixum',
    ),
    'plixum_item_admin' => array(
      'variables' => array(
        'key' => NULL,
        'item' => array(),
        'content' => NULL,
      ),
      'file' => 'includes/plixum.theme.inc',
    ),
    'plixum_content_admin' => array(
      'variables' => array('content' => NULL),
      'file' => 'includes/plixum.theme.inc',
    ),
  );

  return $themes;
}

/**
 * Implements hook_block_info().
 *
 * This hook declares to Drupal what blocks are provided by the module.
 */
function _plixum_block_info() {
  $blocks = array();

  $result = db_query('SELECT bid, info FROM {plixum} ORDER BY info');
  foreach ($result as $block) {
    $blocks[$block->bid]['info'] = check_plain($block->info);
    // Not worth caching.
    $blocks[$block->bid]['cache'] = DRUPAL_NO_CACHE;
  }

  return $blocks;
}

/**
 * Implements hook_block_configure().
 */
function _plixum_block_configure($delta = 0) {
  if ($delta) {
    $form = plixum_block_get_delta($delta);
  }
  else {
    $form = array();
  }

  return plixum_block_form($form);
}

/**
 * Implements hook_block_save().
 */
function _plixum_block_save($delta = 0, $edit = array()) {
  $block                   = block_load('plixum', $delta);
  $data['plixum_path']     = $edit['plixum_path'];
  $data['plixum_preset']   = $edit['plixum_preset'];
  $data['plixum_position'] = $edit['plixum_position'];
  $data['title']           = $block->title;
  $data['classes']         = $edit['classes'];

  db_update('plixum')
  ->fields(array(
    'body' => $edit['body']['value'],
    'info' => $edit['info'],
    'format' => $edit['body']['format'],
    'options' => serialize($data),
  ))
  ->condition('bid', $delta)
  ->execute();

  return TRUE;
}

/**
 * Form constructor for the plixum block form.
 *
 * @param array $edit
 *   (optional) An associative array of information retrieved by
 *   plixum_block_get_delta() if an existing block is being edited, or an empty
 *   array otherwise. Defaults to array().
 *
 * @ingroup forms
 */
function plixum_block_form($edit = array()) {
  $path = drupal_get_path('module', 'plixum');

  $edit += array(
    'info' => '',
    'body' => '',
  );

  $options = unserialize($edit['options']);

  $form['plixum_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Block settings'),
  );

  $form['plixum_settings']['#attached']['js'][]  = $path . '/js/lorem.min.js';
  $form['plixum_settings']['#attached']['js'][]  = $path . '/js/plixum.js';
  $form['plixum_settings']['#attached']['css'][] = $path . '/css/plixum.admin.css';

  $form['plixum_settings']['info'] = array(
    '#type' => 'textfield',
    '#title' => t('Block description'),
    '#default_value' => $edit['info'],
    '#maxlength' => 64,
    '#description' => t('A unique brief description of your block. Used on the <a href="@overview">Blocks administration page</a>.', array('@overview' => url('admin/structure/block'))),
    '#required' => TRUE,
    '#weight' => -18,
  );

  $form['plixum_settings']['body'] = array(
    '#type' => 'text_format',
    '#title' => t('Block body'),
    '#default_value' => $edit['body'],
    '#format' => isset($edit['format']) ? $edit['format'] : NULL,
    '#rows' => 15,
    '#field_suffix' => '<button id="plixum-button" class="button primary">' . t('Reset &amp; Randomize') . '</button>',
  );

  $form['plixum_settings']['plixum_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to image'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => isset($options['plixum_path']) ? $options['plixum_path'] : NULL,
    '#description' => t("The path to image file relative to the site's base path. If image library available, populate this field by clicking an image from the library."),
    '#attributes' => array('class' => array('plixum-image-target')),
  );

  $form['plixum_settings']['plixum_preset'] = array(
    '#type' => 'select',
    '#title' => t('Image style'),
    '#default_value' => isset($options['plixum_preset']) ? $options['plixum_preset'] : NULL,
    '#options' => image_style_options(TRUE),
    '#description' => t('Choose the image style. Add more <a href="@image_style">image styles</a> via administration area.', array('@image_style' => url('admin/config/media/image-styles'))),
  );

  $form['plixum_settings']['plixum_position'] = array(
    '#type' => 'select',
    '#title' => t('Image position'),
    '#default_value' => isset($options['plixum_position']) ? $options['plixum_position'] : NULL,
    '#options' => array(
      'top' => t('Top'),
      'right' => t('Right'),
      'bottom' => t('Bottom'),
      'left' => t('Left'),
      'absolute' => t('Absolute'),
    ),
    '#empty_option' => t('- None -'),
    '#description' => t('Choose the image position relative to block body.'),
  );

  // Do we have an image library.
  if (plixum_get_settings('plixum_image_library') && $images = plixum_scan_images()) {
    $selected = isset($options['plixum_path']) ? $options['plixum_path'] : '';

    // Check if any image already in use to give hint on image selection.
    if ($blocks = db_query('SELECT options, bid FROM {plixum} ORDER BY bid')) {
      foreach ($blocks as $block) {
        $options_all = unserialize($block->options);
        $selected_images[$block->bid] = $options_all['plixum_path'];
      }
    }

    $items = array();
    foreach ($images as $key => $file) {
      $uri     = $file->uri;
      $is_used = FALSE;
      $current = FALSE;
      if (is_array($selected_images) && in_array($uri, $selected_images)) {
        $is_used = TRUE;
        if (!empty($selected) && $uri == $selected) {
          $current = TRUE;
        }
      }

      $thumbnail = theme('image_style', array(
        'style_name' => 'plixum',
        'path' => $uri,
        'getsize' => TRUE,
        'attributes' => array(
          'title' => '',
          'alt' => check_plain($file->name),
          'class' => 'plixum-image-preview',
        ),
      ));

      $attributes['html'] = TRUE;
      $attributes['attributes']['class'] = array('plixum-insert-image');
      $attributes['attributes']['title'] = t('Click to insert image to the path field!');
      if ($is_used) {
        $attributes['attributes']['class'][] = 'selected';
        $attributes['attributes']['title'] = t('Already selected');
      }
      if ($current) {
        $attributes['attributes']['class'][] = 'current';
        $attributes['attributes']['title'] = t('Current image');
        $attributes['attributes']['weight'][] = -99;
      }

      $attributes['attributes']['data-storage'] = $uri;
      $attributes['html'] = TRUE;
      $item = l($thumbnail, file_create_url($uri), $attributes);
      $items[$uri] = $item;
    }

    $preview_images = theme('item_list', array(
      'items' => $items,
      'title' => NULL,
      'type' => 'ul',
      'attributes' => array('class' => 'plixum-image-list clearfix'),
    ));

    // Display images for selection.
    $form['plixum_settings']['plixum_images'] = array(
      '#type' => 'item',
      '#markup' => $preview_images,
      '#description' => t('Click image to insert it into the image path field.'),
    );
  }

  $form['plixum_settings']['classes'] = array(
    '#type' => 'textfield',
    '#title' => t('Additional dummy classes for Plixum block.'),
    '#maxlength' => 128,
    '#default_value' => isset($options['classes']) ? $options['classes'] : NULL,
    '#description' => t('Additional dummy classes to customize the block. This, like all Plixum things, will be deleted on uninstall, but you keep your classes at CSS file which later can be referenced via block_class.module at production.'),
  );

  // Includes HTML elements for insertion into textarea.
  // Credits to styleguide.module which we can not depend on for the HTML code.
  module_load_include('inc', 'plixum', 'includes/plixum.plixum');
  $form['plixum_settings']['plixum_elements'] = array(
    '#type' => 'fieldset',
    '#title' => t('HTML elements'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Click each HTML element block to insert into the textarea.'),
  );

  $form['plixum_settings']['plixum_elements']['plixum_markup_elements'] = array(
    '#type' => 'item',
    '#markup' => plixum_html_elements(),
  );

  return $form;
}
