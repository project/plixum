<?php
/**
 * @file
 * Default theme implementation for the Plixum block.
 *
 * Available variables:
 * - $content: The renderable array containing the plixum block.
 *
 * The following variables are provided for contextual information.
 * - $config: An array of the block's configuration settings. Includes
 *   - plixum_path: The path to image.
 *   - plixum_preset: The preset or image_style.
 *   - plixum_position: Th eposition of the image, can be:
 *     - top
 *     - right
 *     - bottom
 *     - left
 *     - absolute: image is absolutely positioned under tyhe the body text.
 *     - classes: the custom classes.
 *
 * @see template_preprocess_plixum()
 */
?>
<?php print render($content); ?>
