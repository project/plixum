<?php

/**
 * @file
 * Provides a way to generate pixum lipsum blocks to rapidly build a prototype.
 *
 * Useful before having a chance to build or code the real production blocks.
 * Like any developer tool, this module should not be enabled at production.
 * Use your own real blocks at production. Once uninstalled, all plixum blocks
 * will be removed cleanly including the copy of images.
 */

/**
 * Off-load the following infrequently called hooks to another file.
 */
require_once dirname(__FILE__) . '/includes/plixum.utilities.inc';

/**
 * Implements hook_theme().
 */
function plixum_theme(&$existing, $type, $theme, $path) {
  module_load_include('inc', 'plixum', 'includes/plixum.admin');
  return _plixum_theme($existing, $type, $theme, $path);
}

/**
 * Implements hook_block_info().
 */
function plixum_block_info() {
  module_load_include('inc', 'plixum', 'includes/plixum.admin');
  return _plixum_block_info();
}

/**
 * Implements hook_block_configure().
 */
function plixum_block_configure($delta = '') {
  module_load_include('inc', 'plixum', 'includes/plixum.admin');
  return _plixum_block_configure($delta);
}

/**
 * Implements hook_block_save().
 */
function plixum_block_save($delta = '', $edit = array()) {
  module_load_include('inc', 'plixum', 'includes/plixum.admin');
  return _plixum_block_save($delta, $edit);
}

/**
 * Implements hook_help().
 */
function plixum_help($path, $arg) {
  if ($path == 'admin/help#plixum') {
    return '<p>' . t('The Plixum module provides a way to generate dummy pixum lipsum blocks to rapidly build a prototype before having a chance to build or code the real production blocks. Visit <a href="@url">admin/config/development/plixum</a> to generate the Plixum blocks, and add local image library. Once the blocks created, you can edit individual block to re-generate lipsum, or add a local pixum (define the image style, and its position). Or insert Drupal HTML elements preset by clicking the relevant HTML block. Like any developer tool, this module should not be enabled at production. Use your own real blocks at production instead.', array('@url' => url('admin/config/development/plixum'))) . '</p>';
  }
}

/**
 * Implements hook_menu().
 */
function plixum_menu() {
  $items = array();

  $items['admin/config/development/plixum'] = array(
    'title' => 'Generate Plixum blocks',
    'description' => 'Configure Plixum.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('plixum_admin_settings'),
    'access callback' => 'user_access',
    'access arguments' => array('administer plixum'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'includes/plixum.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function plixum_permission() {
  return array(
    'administer plixum' => array(
      'title' => t('Administer plixum'),
      'description' => t('Manage settings for the Plixum module'),
    ),
  );
}

/**
 * Implements hook_block_view().
 */
function plixum_block_view($delta = '') {
  static $already_added = FALSE;

  $config = plixum_get_config($delta);

  $data = array();
  if (($content = plixum_build_plixum($config)) !== NULL) {
    $data['subject'] = NULL;
    $data['content']['#content'] = $content;
    $data['content']['#theme'][] = 'plixum';
    $data['content']['#config']  = $config['config'];
    $data['content']['#delta']   = $delta;
    if (!$already_added) {
      $already_added = TRUE;
      drupal_add_css(drupal_get_path('module', 'plixum') . '/css/plixum.css');
    }
  }

  return $data;
}

/**
 * HTML for the Plixum block.
 *
 * @param array $config
 *   The array of config that uniquely identifies the Plixum block.
 *
 * @return string
 *   HTML for a Plixum block.
 */
function plixum_build_plixum($config = array()) {
  $build   = '';
  $image   = '';
  $content = '';
  if ($config) {
    $options = $config['config'];
    $content['title'] = isset($options['title']) && $options['title'] != '<none>' ? check_plain($options['title']) : '';
    $content['body']  = check_markup($config['body'], $config['format'], '', TRUE);
    unset($config['body']);

    if (!empty($options['plixum_path'])) {
      $content['plixum_path'] = !empty($options['plixum_path']) ? check_plain($options['plixum_path']) : '';
      $content['plixum_preset'] = !empty($options['plixum_preset']) ? check_plain($options['plixum_preset']) : 'medium';
      $content['plixum_position'] = !empty($options['plixum_position']) ? check_plain($options['plixum_position']) : 'top';

      // The image.
      $image_attributes = array(
        'style_name' => $content['plixum_preset'],
        'path' => $content['plixum_path'],
        'getsize' => TRUE,
        'attributes' => array(
          'title' => $content['title'],
          'alt' => 'plixum-image',
        ),
      );
      $thumbnail = theme('image_style', $image_attributes);

      // The linked image.
      $attributes['html'] = TRUE;
      if (function_exists('colorbox_get_path')) {
        $attributes['attributes']['class'][] = 'colorbox';
      }
      // @todo add option to link image to.
      $preview_url = image_style_url('large', $content['plixum_path']);
      $linked_image = l($thumbnail, $preview_url, $attributes);

      $image = '<figure class="plixum-image">' . $linked_image . '</figure>';
    }
    $body = '<div class="plixum-body">' . $content['body'] . '</div>';

    // Render body text and image based on position.
    $element = $image . $body;

    if (isset($content['plixum_position']) && ($content['plixum_position'] == 'bottom' || $content['plixum_position'] == 'absolute')) {
      $element = $body . $image;
    }
    $build = $element;
  }

  return $build;
}

/**
 * Implements hook_form_alter().
 */
function plixum_form_block_admin_configure_alter(&$form, &$form_state) {
  if ($form['module']['#value'] == 'plixum') {
    $form['#validate'][] = 'plixum_block_admin_configure_validate';
  }
}

/**
 * Form validation handler for block_admin_configure().
 */
function plixum_block_admin_configure_validate($form, &$form_state) {
  if ($form_state['values']['module'] == 'plixum') {
    $plixum_exists = (bool) db_query_range('SELECT 1 FROM {plixum} WHERE bid <> :bid AND info = :info', 0, 1, array(
      ':bid' => $form_state['values']['delta'],
      ':info' => $form_state['values']['info'],
    ))->fetchField();

    // Make sure to have unique block info.
    if (empty($form_state['values']['info']) || $plixum_exists) {
      form_set_error('info', t('Ensure that each block description is unique.'));
    }
  }
}

/**
 * Process variables for plixum block content.
 *
 * @see ultimenu.tpl.php
 */
function template_preprocess_plixum(&$variables) {
  $element = $variables['element'];
  $variables['config'] = $element['#config'];
  $variables['delta'] = $element['#delta'];

  // Create the $content variable that templates expect.
  $variables['content'] = $element['#content'];
}

/**
 * Implements hook_preprocess_block().
 */
function plixum_preprocess_block(&$variables) {
  if ($variables['block']->module == 'plixum' && $variables['config'] = $variables['elements']['#config']) {

    $config = $variables['config'];

    // Add class based on image position.
    if (($position = $config['plixum_position']) !== NULL) {
      $variables['classes_array'][] = drupal_html_class('plixum-image-' . $position);
    }

    // Add class based on custom dummy classes.
    if (($classes = $config['classes']) !== NULL) {
      $classes = array_map('trim', explode(" ", $classes));
      // Some blocks may have no wrappers.
      $variables['classes_array'] = $variables['classes_array'] ? array_merge($variables['classes_array'], $classes) : $classes;
    }
  }
}

/**
 * The plixum data for the requested block delta.
 *
 * @param string $delta
 *   The delta that uniquely identifies the block in the block system.
 *
 * @return array
 *   A associative array containing configuration options, else empty array.
 */
function plixum_get_config($delta = NULL) {
  $config = array();

  if ($delta) {
    // Get the block object.
    if ($block = db_query('SELECT * FROM {plixum} WHERE bid = :bid', array(':bid' => $delta))->fetchObject()) {
      $config['body']   = $block->body;
      $config['format'] = $block->format;
      $config['config'] = unserialize($block->options);
    }
  }

  return $config;
}

/**
 * Returns information from database about a plixum block.
 *
 * @param string $delta
 *   ID of the block to get information for.
 *
 * @return array
 *   Associative array of information stored in the database for this block.
 *   Array keys:
 *   - bid: Block ID.
 *   - info: Block description.
 *   - body: Block contents.
 *   - format: Filter ID of the filter format for the body.
 *   - options: Array consists of:
 *     - plixum_path: Path to image file.
 *     - plixum_preset: Preset of the image style.
 *     - plixum_position: Position of the image relative to body text.
 *     - classes: Dummy block classes for quick CSS reference.
 */
function plixum_block_get_delta($delta) {
  return db_query("SELECT * FROM {plixum} WHERE bid = :bid", array(':bid' => $delta))->fetchAssoc();
}
