<?php

/**
 * @file
 * Various HTML elements copied from styleguide.module and modified.
 */

/**
 * Implements hook_plixum().
 */
function plixum_plixum() {
  $items['a'] = array(
    'label' => t('Link'),
    'content' => ucfirst(plixum_greeking(3, FALSE)) . ' ' . l(plixum_greeking(3, FALSE), 'node') . ' ' . plixum_greeking(4, FALSE) . '.',
  );
  $items['em'] = array(
    'label' => t('Emphasis'),
    'content' => ucfirst(plixum_greeking(3, FALSE)) . ' <em>' . plixum_greeking(3, FALSE) . '</em> ' . plixum_greeking(4, FALSE) . '.',
  );
  $items['strong'] = array(
    'label' => t('Strong'),
    'content' => ucfirst(plixum_greeking(3, FALSE)) . ' <strong>' . plixum_greeking(3, FALSE) . '</strong> ' . plixum_greeking(4, FALSE) . '.',
  );
  $items['ul'] = array(
    'label' => t('Unordered list'),
    'theme' => 'item_list',
    'variables' => array('items' => plixum_list(), 'type' => 'ul'),
    'group' => t('Lists'),
  );
  $items['ol'] = array(
    'label' => t('Ordered list'),
    'theme' => 'item_list',
    'variables' => array('items' => plixum_list(), 'type' => 'ol'),
    'group' => t('Lists'),
  );
  $items['ul_title'] = array(
    'label' => t('Unordered list, with title'),
    'theme' => 'item_list',
    'variables' => array(
      'items' => plixum_list(),
      'type' => 'ul',
      'title' => ucwords(plixum_greeking(1, FALSE))),
    'group' => t('Lists'),
  );
  $items['ol_title'] = array(
    'label' => t('Ordered list, with title'),
    'theme' => 'item_list',
    'variables' => array(
      'items' => plixum_list(),
      'type' => 'ol',
      'title' => ucwords(plixum_greeking(1, FALSE))),
    'group' => t('Lists'),
  );
  $items['menu_tree'] = array(
    'label' => t('Menu tree'),
    'content' => menu_tree('management'),
    'group' => t('Menus'),
  );
  $items['table'] = array(
    'label' => t('Table'),
    'theme' => 'table',
    'variables' => array('header' => plixum_header(), 'rows' => plixum_rows()),
    'group' => t('Tables'),
  );
  $items['text'] = array(
    'label' => t('Text block'),
    'content' => plixum_greeking(3),
    'group' => t('Text'),
  );
  $items['image-horizontal'] = array(
    'label' => t('Image, horizontal'),
    'theme' => 'image',
    'variables' => array(
      'path' => plixum_image('horizontal'),
      'alt' => t('My image'),
      'title' => t('My image')),
    'group' => t('Media'),
  );
  $items['image-vertical'] = array(
    'label' => t('Image, vertical'),
    'theme' => 'image',
    'variables' => array(
      'path' => plixum_image('vertical'),
      'alt' => t('My image'),
      'title' => t('My image')),
    'group' => t('Media'),
  );
  $items['image-inset-horizontal'] = array(
    'label' => t('Image, horizontal, within text'),
    'content' => plixum_greeking(1) . theme('image', array(
      'path' => plixum_image('horizontal'),
      'alt' => t('My image'),
      'title' => t('My image'))) . plixum_greeking(1),
    'group' => t('Media'),
  );
  $items['image-inset-vertical'] = array(
    'label' => t('Image, vertical, within text'),
    'content' => plixum_greeking(1) . theme('image', array(
      'path' => plixum_image('vertical'),
      'alt' => t('My image'),
      'title' => t('My image'))) . plixum_greeking(1),
    'group' => t('Media'),
  );
  $content = '';
  for ($i = 1; $i <= 6; $i++) {
    $content .= "<h$i>h$i: " . implode(' ', plixum_list()) . "</h$i>";
  }
  $items['headings'] = array(
    'label' => "Headings",
    'content' => $content,
    'group' => t('Text'),
  );
  $content = '';
  for ($i = 1; $i <= 6; $i++) {
    $content .= "<h$i>" . "h$i: " . implode(' ', plixum_list()) . "</h$i>" . plixum_greeking(5);
  }
  $items['headings_text'] = array(
    'label' => "Headings with text",
    'content' => $content,
    'group' => t('Text'),
  );
  $messages = array('status', 'warning', 'error');
  foreach ($messages as $message) {
    $items[$message . '-message'] = array(
      'label' => ucwords($message) . ' message',
      'content' => '<div class="messages ' . $message . '"><h2 class="element-invisible">Status message</h2>' . plixum_greeking(8) . '</div>',
    );
  }

  return $items;
}

/**
 * A helper function to build dummy html elements.
 *
 * @return string
 *   Drupal HTML elements.
 */
function plixum_html_elements() {
  $items = module_invoke_all('plixum');
  drupal_alter('plixum', $items);

  $groups = array();
  foreach ($items as $key => $item) {
    if (!isset($item['group'])) {
      $item['group'] = t('Common');
    }
    else {
      $item['group'] = t('@group', array('@group' => $item['group']));
    }
    $item['label'] = t('@label', array('@label' => $item['label']));
    $groups[$item['group']][$key] = $item;
  }
  ksort($groups);

  $output = '';
  $content = '';
  // Process the elements, by group.
  foreach ($groups as $group => $elements) {
    foreach ($elements as $key => $item) {
      $display = '';
      // Output a standard theme item.
      if (isset($item['theme'])) {
        $display = theme($item['theme'], $item['variables']);
      }
      // Output a standard HTML tag. In Drupal 7, the preference
      // is to pass theme('html_tag') instead. This is kept for API
      // compatibility with Drupal 6.
      elseif (isset($item['tag']) && isset($item['content'])) {
        if (empty($item['attributes'])) {
          $display = '<' . $item['tag'] . '>' . $item['content'] . '</' . $item['tag'] . '>';
        }
        else {
          $display = '<' . $item['tag'] . ' ' . drupal_attributes($item['attributes']) . '>' . $item['content'] . '</' . $item['tag'] . '>';
        }
      }
      // Support a renderable array for content.
      elseif (isset($item['content']) && is_array($item['content'])) {
        $display = drupal_render($item['content']);
      }
      // Just print the provided content.
      elseif (isset($item['content'])) {
        $display = $item['content'];
      }
      // Add the content.
      $content .= theme('plixum_item_admin', array(
        'key' => $key,
        'item' => $item,
        'content' => $display));
    }
  }
  $output .= theme('plixum_content_admin', array('content' => $content));
  return $output;
}


/**
 * Provide a default image for display.
 *
 * Images should be in the assets directory. The current images are
 * (c) Gaus Surahman and used by permission under GPL.
 *
 * @param string $image
 *   The name of the image. Will be prefixed with 'image-'.
 *
 * @param string $type
 *   The file type, (jpg, png, gif). Do not include a dot.
 *
 * @return string
 *   The Drupal path to the file.
 */
function plixum_image($image = 'vertical', $type = 'jpg') {
  $path = drupal_get_path('module', 'plixum');
  $filepath = $path . '/assets/image-' . $image . '.' . $type;
  if (file_exists($filepath)) {
    return $filepath;
  }
}

/**
 * Return a simple array of words.
 *
 * @param int $size
 *   The size of the list to return.
 *
 * @return array
 *   An array of words.
 */
function plixum_list($size = 5) {
  $items = array();
  for ($i = 0; $i < $size; $i++) {
    $items[] = plixum_greeking(8);
  }
  return $items;
}

/**
 * Return a random table header array.
 *
 * @param int $size
 *   The size of the list to return.
 *
 * @return array
 *   An array of header elements.
 */
function plixum_header($size = 5) {
  $header = plixum_list($size);
  return $header;
}

/**
 * Return a random table row array.
 *
 * @param int $size
 *   The size of the list to return.
 *
 * @return array
 *   An array of row elements.
 */
function plixum_rows($size = 5) {
  $rows = array();
  for ($i = 0; $i < $size; $i++) {
    $rows[] = plixum_list($size);
  }
  return $rows;
}

/**
 * Sample form, showing all elements.
 */
function plixum_form($form, &$form_state, $form_keys = array()) {
  $form = array();
  $options = array();
  $list = plixum_list();
  foreach ($list as $item) {
    $options[$item] = $item;
  }
  $form['select'] = array(
    '#type' => 'select',
    '#title' => t('Select'),
    '#options' => $options,
    '#description' => plixum_greeking(1),
  );
  $form['checkbox'] = array(
    '#type' => 'checkbox',
    '#title' => t('Checkbox'),
    '#value' => 1,
    '#default_value' => 1,
    '#description' => plixum_greeking(1),
  );
  $form['checkboxes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Checkboxes'),
    '#options' => $options,
    '#description' => plixum_greeking(1),
  );
  $form['radios'] = array(
    '#type' => 'radios',
    '#title' => t('Radios'),
    '#options' => $options,
    '#description' => plixum_greeking(1),
  );
  $form['textfield'] = array(
    '#type' => 'textfield',
    '#title' => t('Textfield'),
    '#default_value' => plixum_greeking(1),
    '#description' => plixum_greeking(1),
  );
  $form['autocomplete'] = array(
    '#type' => 'textfield',
    '#title' => t('Autocomplete textfield'),
    '#default_value' => plixum_greeking(1),
    '#description' => plixum_greeking(1),
    '#autocomplete_path' => 'user/autocomplete',
  );
  $form['textfield-machine'] = array(
    '#type' => 'textfield',
    '#title' => t('Textfield, with machine name'),
    '#default_value' => plixum_greeking(1) . ' ' . plixum_greeking(1) . ' ' . plixum_greeking(1),
    '#description' => plixum_greeking(1),
  );
  $form['machine_name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Machine name'),
    '#machine_name' => array(
      'source' => array('textfield-machine'),
    ),
    '#description' => plixum_greeking(1),
  );
  $form['textarea'] = array(
    '#type' => 'textarea',
    '#title' => t('Textarea'),
    '#default_value' => plixum_greeking(1),
    '#description' => plixum_greeking(1),
  );
  $form['date'] = array(
    '#type' => 'date',
    '#title' => t('Date'),
    '#description' => plixum_greeking(1),
  );
  $form['file'] = array(
    '#type' => 'file',
    '#title' => t('File'),
    '#description' => plixum_greeking(1),
  );
  $form['managed_file'] = array(
    '#type' => 'managed_file',
    '#title' => t('Managed file'),
    '#description' => plixum_greeking(1),
  );
  $form['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#default_value' => plixum_greeking(1),
    '#description' => plixum_greeking(1),
  );
  $form['password_confirm'] = array(
    '#type' => 'password_confirm',
    '#title' => t('Password confirm'),
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#delta' => 10,
    '#description' => plixum_greeking(1),
  );
  $form['fieldset-collapsed'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fieldset collapsed'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => plixum_greeking(1),
  );
  $form['fieldset-collapsible'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fieldset collapsible'),
    '#collapsible' => TRUE,
    '#description' => plixum_greeking(1),
  );
  $form['fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fieldset'),
    '#collapsible' => FALSE,
    '#description' => plixum_greeking(1),
  );
  $fieldsets = array('fieldset', 'fieldset-collapsed', 'fieldset-collapsible');
  $count = 0;
  foreach ($form as $key => $value) {
    if ($value['#type'] != 'fieldset' && $value['#type'] != 'checkbox' && $count < 2) {
      $count++;
      foreach ($fieldsets as $item) {
        $form[$item][$key . '-' . $item] = $value;
      }
    }
  }
  $form['vertical_tabs'] = array(
    '#type' => 'vertical_tabs',
  );
  foreach ($fieldsets as $fieldset) {
    $form['vertical_tabs'][$fieldset] = $form[$fieldset];
  }
  $form['markup'] = array(
    '#markup' => t('<p><em>Markup</em>: Note that markup does not allow titles or descriptions. Use "item" for those options.</p>') . plixum_greeking(1),
  );
  $form['item'] = array(
    '#type' => 'item',
    '#title' => t('Item'),
    '#markup' => plixum_greeking(1),
    '#description' => plixum_greeking(1),
  );
  $form['image_button'] = array(
    '#type' => 'image_button',
    '#src' => 'misc/druplicon.png',
    '#attributes' => array('height' => 20),
    '#name' => t('Image button'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  $form['button'] = array(
    '#type' => 'button',
    '#value' => t('Button'),
  );
  if (!empty($form_keys)) {
    $items = array();
    foreach ($form_keys as $key) {
      if (isset($form[$key])) {
        $items[$key] = $form[$key];
      }
    }
    return $items;
  }
  return $form;
}
