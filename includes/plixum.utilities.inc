<?php

/**
 * @file
 * Misc functions that hardly change for Plixum module.
 */

/**
 * A wrapper function for variable_get.
 *
 * Allows storing multiple variables in one place rather than setting and
 * calling each.
 *
 * @param array|string $setting_name
 *   The name of settings which can be an array or string.
 *
 * @return string|NULL
 *   Returns the value if found, or else $default.
 */
function plixum_get_settings($setting_name = NULL, $default = NULL) {
  $cache = &drupal_static(__FUNCTION__);

  if (empty($cache)) {
    $settings = variable_get('plixum_settings', array());
    if (isset($settings[$setting_name]) && ($setting = $settings[$setting_name]) !== NULL) {
      return $setting;
    }
  }
  else {
    if (isset($cache[$setting_name])) {
      return $cache[$setting_name];
    }
  }

  return $default;
}

/**
 * Implements hook_image_default_styles().
 */
function plixum_image_default_styles() {
  $styles = array();

  // Dummy thumbnail for use at block admin.
  $styles['plixum'] = array(
    'effects' => array(
      array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 80,
          'height' => 80,
        ),
        'weight' => 0,
      ),
    ),
  );

  return $styles;
}

/**
 * Check if folder is available or create it.
 *
 * @param string $directory
 *   Folder to check.
 */
function plixum_check_dir($directory) {
  // Normalize directory name.
  $directory = file_stream_wrapper_uri_normalize($directory);

  // Create directory (if not exist).
  $check = file_prepare_directory($directory, FILE_CREATE_DIRECTORY);
  return $check;
}

/**
 * Stole this from Design stealing from Devel Generate and modified.
 */
function plixum_greeking($word_count, $sentences = TRUE) {
  static $dictionary = array(
    'abbas',
    'abdo',
    'abico',
    'abigo',
    'abluo',
    'accumsan',
    'acsi',
    'ad',
    'adipiscing',
    'aliquam',
    'aliquip',
    'amet',
    'antehabeo',
    'appellatio',
    'aptent',
    'at',
    'augue',
    'autem',
    'bene',
    'blandit',
    'brevitas',
    'caecus',
    'camur',
    'capto',
    'causa',
    'cogo',
    'comis',
    'commodo',
    'commoveo',
    'consectetuer',
    'consequat',
    'conventio',
    'cui',
    'damnum',
    'decet',
    'defui',
    'diam',
    'dignissim',
    'distineo',
    'dolor',
    'dolore',
    'dolus',
    'duis',
    'ea',
    'eligo',
    'elit',
    'enim',
    'erat',
    'eros',
    'esca',
    'esse',
    'et',
    'eu',
    'euismod',
    'eum',
    'ex',
    'exerci',
    'exputo',
    'facilisi',
    'facilisis',
    'fere',
    'feugiat',
    'gemino',
    'genitus',
    'gilvus',
    'gravis',
    'haero',
    'hendrerit',
    'hos',
    'huic',
    'humo',
    'iaceo',
    'ibidem',
    'ideo',
    'ille',
    'illum',
    'immitto',
    'importunus',
    'imputo',
    'in',
    'incassum',
    'inhibeo',
    'interdico',
    'iriure',
    'iusto',
    'iustum',
    'jugis',
    'jumentum',
    'jus',
    'laoreet',
    'lenis',
    'letalis',
    'lobortis',
    'loquor',
    'lucidus',
    'luctus',
    'ludus',
    'luptatum',
    'macto',
    'magna',
    'mauris',
    'melior',
    'metuo',
    'meus',
    'minim',
    'modo',
    'molior',
    'mos',
    'natu',
    'neo',
    'neque',
    'nibh',
    'nimis',
    'nisl',
    'nobis',
    'nostrud',
    'nulla',
    'nunc',
    'nutus',
    'obruo',
    'occuro',
    'odio',
    'olim',
    'oppeto',
    'os',
    'pagus',
    'pala',
    'paratus',
    'patria',
    'paulatim',
    'pecus',
    'persto',
    'pertineo',
    'plaga',
    'pneum',
    'populus',
    'praemitto',
    'praesent',
    'premo',
    'probo',
    'proprius',
    'quadrum',
    'quae',
    'qui',
    'quia',
    'quibus',
    'quidem',
    'quidne',
    'quis',
    'ratis',
    'refero',
    'refoveo',
    'roto',
    'rusticus',
    'saepius',
    'sagaciter',
    'saluto',
    'scisco',
    'secundum',
    'sed',
    'si',
    'similis',
    'singularis',
    'sino',
    'sit',
    'sudo',
    'suscipere',
    'suscipit',
    'tamen',
    'tation',
    'te',
    'tego',
    'tincidunt',
    'torqueo',
    'tum',
    'turpis',
    'typicus',
    'ulciscor',
    'ullamcorper',
    'usitas',
    'ut',
    'utinam',
    'utrum',
    'uxor',
    'valde',
    'valetudo',
    'validus',
    'vel',
    'velit',
    'veniam',
    'venio',
    'vereor',
    'vero',
    'verto',
    'vicis',
    'vindico',
    'virtus',
    'voco',
    'volutpat',
    'vulpes',
    'vulputate',
    'wisi',
    'ymo',
    'zelus',
  );

  $greeking = '';

  // Create a stack containing the requested amount of random words.
  $words = array();
  for ($i = 0; $i < $word_count; $i++) {
    $words[] = $dictionary[array_rand($dictionary)];
  }
  // If more than 10 words have been requested, build sentences.
  $chunks = array();
  if ($word_count > 10) {
    // Some variance in sentence length.
    $sentence_length = range(4, 30);
    while ($words) {
      $chunk = array_splice($words, 0, array_rand($sentence_length));
      // Only add non-empty chunks.
      if (!empty($chunk)) {
        $chunks[] = $chunk;
      }
    }
  }
  else {
    $chunks = array($words);
  }
  foreach ($chunks as $chunk) {
    $greeking .= ucfirst(implode(' ', $chunk));
    if ($sentences) {
      $greeking .= '. ';
    }
  }

  return $greeking;
}

/**
 * A helper function to return all found images from specified directories.
 *
 * Scans and return available images as a list.
 *
 * @return array
 *   An array of available images, else empty array.
 */
function plixum_scan_images() {
  $images = &drupal_static(__FUNCTION__);
  if (!isset($images)) {
    $images  = array();

    // We already created the directory at install, here for fail safe.
    $storage = file_default_scheme() . '://plixum/images';
    if (!is_dir($storage)) {
      plixum_check_dir($storage);
    }

    $mask = '/\.(jpg|gif|png?)$/';
    if (is_dir($storage)) {
      foreach (file_scan_directory($storage, $mask) as $file) {
        $uri = $file->uri;
        $images[] = $file;
      }
      asort($images);
    }
  }

  return $images;
}

/**
 * A helper function to copy all found images in specified directories.
 *
 * Scans and copy from the source if $source is defined, and store them into
 * public directory.
 *
 * @param string $source
 *   Folder to copy images from into public directory.
 *
 * @return array
 *   If $source, copy from the source, else empty array.
 */
function plixum_copy_images($source = NULL) {
  $images = &drupal_static(__FUNCTION__);
  if (!isset($images)) {
    $images  = array();

    // We already created the directory at install, here for fail safe.
    $storage = file_default_scheme() . '://plixum/images';
    if (!is_dir($storage)) {
      plixum_check_dir($storage);
    }

    $mask = '/\.(jpg|gif|png?)$/';
    if (!empty($source) && is_dir($source)) {
      foreach (file_scan_directory($source, $mask) as $file) {
        $uri = $file->uri;
        $filename = basename($uri);
        // Clean up file names.
        if (function_exists('transliteration_clean_filename')) {
          module_load_include('inc', 'transliteration');
          $filename = transliteration_clean_filename($filename);
        }
        else {
          $filename = drupal_clean_css_identifier($filename,
            array(
              ' ' => '-',
              '_' => '-',
              '/' => '-',
              '[' => '-',
              ']' => '',
              '@' => '-',
            )
          );
          $filename = drupal_strtolower($filename);
        }

        $destination = $storage . '/' . $filename;
        file_unmanaged_copy($uri, $destination, FILE_EXISTS_REPLACE);
      }
    }
  }

  return $images;
}
