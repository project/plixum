/**
 * @file plixum.js
 */
(function ($, Drupal, window, document, undefined) {
  "use strict";
  Drupal.plixum = Drupal.plixum || {};

  Drupal.behaviors.plixum = {
    attach: function (context, settings) {

      var container = '#edit-plixum-settings',
        $body = $('#edit-body-value', container);

      $('#plixum-button').click(function () {
        Drupal.plixum.disableWYSIWYG();
        $body.attr('data-lorem', 'p' + Math.floor((Math.random() * 12) + 1)).ipsum();
        $(this).html('Reset &amp; Regenerate!');
        return false;
      });

      $('.plixum-insert-image', container).click(function () {
        $('.plixum-image-target', container).val($(this).data('storage'));
        $('.plixum-insert-image').removeClass('active');
        $(this).addClass('active');
        return false;
      });

      $('div.plixum').hover(function () {
        $(this).addClass('plixum-hover').siblings().removeClass('plixum-hover');
      },
      function() {
        $(this).removeClass('plixum-hover');
      })
      .click(function () {
        var curVal = $body.val(),
          newVal = curVal + $(this).html();
        Drupal.plixum.disableWYSIWYG();

        $body.val(newVal);
        Drupal.plixum.scrollJump(container);
      });
    }
  };
  
  // Temporarily disable WYSWIG to insert HTML into textarea.
  Drupal.plixum.disableWYSIWYG = function () {
    var $wysiwygToggle = $('#wysiwyg-toggle-edit-body-value');
    if ($wysiwygToggle.text() === 'Disable rich-text') {
       $wysiwygToggle.click();
    }
  }

  // Jump to  the top of block form.
  Drupal.plixum.scrollJump = function (id) {
    $('html, body').animate({
      scrollTop: $(id).offset().top - 0
    }, 800);
  }

})(jQuery, Drupal, this, this.document);