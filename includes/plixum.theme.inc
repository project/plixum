<?php

/**
 * @file
 * Theme file for the Plixum module.
 */

/**
 * Themes a display item.
 *
 * @param array $variables
 *   The theme variables array, including:
 *   -- 'key' a unique string indicating the name of the item.
 *   -- 'item' an array of data about the item.
 *   -- 'content' the content to display for this item.
 * Borrows with gratitude from Styleguide to build HTML elements at block admin.
 */
function theme_plixum_item_admin($variables) {
  $key = $variables['key'];
  $item = $variables['item'];
  $content = $variables['content'];
  $build = '';
  $build .= "<a name=\"$key\"></a>";
  $build .= '<h2 class="plixum">' . $item['label'] . '</h2>';
  if (!empty($item['description'])) {
    $build .= '<p class="plixum-description">';
    $build .= $item['description'];
    $build .= '</p>';
  }
  $build .= '<div class="plixum" title="Click to insert into textarea!">';
  $build .= $content;
  $build .= '</div>';
  return $build;
}

/**
 * Themes the content.
 *
 * This function is here in case anyone wants to change it.
 *
 * @param array $variables
 *   The theme variables array, including:
 *   - 'content' an HTML content element.
 */
function theme_plixum_content_admin($variables) {
  return $variables['content'];
}
