The Plixum&trade; module is a slip of the tongue for Pixum Lipsum.

A development tool to generate pixum lipsum blocks and allow a theme developer
or site builder to rapidly build dummy static blocks during prototyping before
having a chance to start coding, or building the real production blocks.

Plixum is the missing piece from the amazing Devel Generate. However it does not
require Devel to function.

Features:
==========
1. Multiple block instances.
2. Regenerate/randomize lorem ipsum text per block instance.
3. Attach local images into block to have a real simulation of design.
4. Image styles and positions: top, right, bottom, left, or absolute.
5. Insert Drupal HTML elements preset into block textarea. If using WYSWYG, you
   may want to enable "Apply source formatting" to re-format the HTML source code.
6. A local image storage or library to pull images out from, available to each
   Plixum block.

Usage:
======
1. Administration > Configuration > Development > Generate Plixum blocks
   admin/config/development/plixum
   a. Generate blocks.
   b. Define path to local image library.
2. Administration > Structure > Blocks
   admin/structure/block
   a. Assign blocks to any regions.
   b. Randomize lipsum text, or append Drupal HTML elements at click.
   c. Select image, image style and image position.

Requirements:
============
1. Drupal core optional image.module should be enabled.

Credits:
=======
1. <a href="http://drupal.org/project/styleguide">Styleguide</a> for some HTML elements code.
2. <a href="http://drupal.org/project/devel">Devel</a> via <a href="http://drupal.org/project/design">Design</a> for lorem ipsum greeking code.
3. Drupal core for some custom block code.

Sandbox:
=======
http://drupal.org/sandbox/gausarts/1893782

Full project:
=============
http://drupal.org/project/plixum
